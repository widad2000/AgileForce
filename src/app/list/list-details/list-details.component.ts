import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { SalesforceService } from 'src/app/salesforce-service.service';
@Component({
  selector: 'app-list-details',
  templateUrl: './list-details.component.html',
  styleUrls: ['./list-details.component.css']
})
export class ListDetailsComponent {
  task !: any;
  list !: any;
  constructor( @Inject(MAT_DIALOG_DATA) public data: { talk: any , list : any },
  private salesforceService: SalesforceService
  ) {}
  ngOnInit(){
    this.task = this.data.talk;
    this.list = this.data.list;
    console.log("my task",this.task);
  }
}
