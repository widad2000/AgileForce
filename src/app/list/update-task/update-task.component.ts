import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { SalesforceService } from 'src/app/salesforce-service.service';
import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {MatChipInputEvent} from '@angular/material/chips';
import { MatChipsModule } from '@angular/material/chips';
import { ElementRef, ViewChild, inject} from '@angular/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatAutocompleteSelectedEvent, MatAutocompleteModule} from '@angular/material/autocomplete';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {MatIconModule} from '@angular/material/icon';
import {NgFor, AsyncPipe} from '@angular/common';
import {MatFormFieldModule} from '@angular/material/form-field';
import {LiveAnnouncer} from '@angular/cdk/a11y';


@Component({
  selector: 'app-update-task',
  templateUrl: './update-task.component.html',
  styleUrls: ['./update-task.component.css']
})
export class UpdateTaskComponent {
  visible = true;
  selectable = true;
  removable = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  Tags: string[] = [];
  Urs !: any ;
  formGroup!: FormGroup;
  issueTypesArrayWithColor = Object.values(appConstants.issueTypeListWithColor);
  PriorityArray = Object.values(appConstants.PriorityList);
  tags: Tag[] = [];
  newTagName: string = '';
  user!: any[];
  projetId !: any;
  
  usersoftask !: any[];
  userList: any[] = [];
  userCtrl = new FormControl('');
  filteredusers!: Observable<any[]>;
  differenceList : any [] = [];

  @ViewChild('userInput')
  usersInput!: ElementRef<HTMLInputElement>;
  announcer = inject(LiveAnnouncer);

  constructor( @Inject(MAT_DIALOG_DATA) public data: { IdTask: any , projetId : any; listId : any },
  public formBuilder: FormBuilder,
  public dialogRef: MatDialogRef<UpdateTaskComponent>,
  public colorPickerdialog: MatDialog,
  private salesforceService: SalesforceService
  ) {}


  ngOnInit() {
    this.Urs = this.data.IdTask;
    this.projetId = this.data.projetId;
    
    this.formGroup = this.formBuilder.group({
     
      text__c	: new FormControl(this.data.IdTask.text__c),
      Name : new FormControl(this.data.IdTask.Name),
      priority__c : this.data.IdTask.priority__c,
      List__c : this.data.listId,
      issueType__c : this.data.IdTask.issueType__c,
      block__c : false,
      done__c : false
      
    });

    //list of userstask "useroftask" & "userlist"
    this.usersoftask = this.Urs.UserTasks__r.records;
    this.usersoftask.forEach((item: any) => {
        if (item.User__r) {
          this.userList.push(item.User__r);
        }
      });
    
    //allusers - user of task  "differenceList"
    this.salesforceService.getInfos(`SELECT User__c , User__r.Name FROM UserTeam__c WHERE Team__c IN (SELECT Team__c FROM Project__c WHERE Id = '${this.projetId}')`).subscribe(
      res=>{
        this.user = res.records;
      }
    );

//------------------------------------------
    this.filteredusers = this.userCtrl.valueChanges.pipe(
      startWith(null),
      map((user: string | any | null) => {
        if (user) {
          const filteredUsers = this.user.filter((u: any) =>
            u.User__r.Name.toLowerCase().includes(user.toLowerCase())
          );
          return filteredUsers;
        } else {
          return this.user;
        }
      })
    );
      console.log(this.userList);
      
  }
  
  private _filter(value: string): any[] {
    const filterValue = value.toLowerCase();
    return this.differenceList.filter(fruit => fruit.toLowerCase().includes(filterValue));
  }
  selected(event: MatAutocompleteSelectedEvent): void {
    this.userList.push(event.option.viewValue);
    this.usersInput.nativeElement.value = '';
    this.userCtrl.setValue(null);
  }
  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();

    // Add our fruit
    if (value) {
      this.userList.push(value);
    }

    // Clear the input value
    event.chipInput!.clear();
    this.userCtrl.setValue(null);
  }

  remove(fruit: string): void {
    const index = this.userList.indexOf(fruit);

    if (index >= 0) {
      this.userList.splice(index, 1);

      this.announcer.announce(`Removed ${fruit}`);
    }
  }

    
     

  onSubmit() {
    const formValues = this.formGroup.value;
    console.log(formValues);
    
    this.salesforceService.updateRecord('Talk__c',this.data.IdTask.Id,formValues).subscribe(
      res=>{
        console.log(res);
        
      });
      this.dialogRef.close();
      this.dialogRef.afterClosed().subscribe(result => {
        window.location.reload();
      
    });
 }
     
}


export interface Tag {
  name: string;
  color?: string;
}



export enum Priority {
  Low = 'Low',
  Lowest = 'Lowest',
  Medium = 'Medium',
  High = 'High',
  Highest = 'Highest'
}


export enum IssueType {
  Task = 'task',
  SubTask = 'sub-task',
  Bug = 'bug',
  Epic = 'epic',
  Story = 'story'
}

export const appConstants = {

  /** Issue Types with ttheir hardcoded colors */
  issueTypeListWithColor: {
      [IssueType.Bug]: {
          name: IssueType.Bug,
          color: '#99333352'
      },
      [IssueType.Epic]: {
          name: IssueType.Epic,
          color: '#33996652'
      },
      [IssueType.Story]: {
          name: IssueType.Story,
          color: '#fff3d4'
      },
      [IssueType.Task]: {
          name: IssueType.Task,
          color: '#99ccff61'
      },
      [IssueType.SubTask]: {
          name: IssueType.SubTask,
          color: '#3d7e9a4d'
      }
  },
  PriorityList: {
    [Priority.Lowest]: {
      name: Priority.Lowest,
      color: '#FFE95D'
      
  },
    [Priority.Low]: {
        name: Priority.Low,
        color: '#FCD432',

       
    },
    
    [Priority.Medium]: {
        name: Priority.Medium,
        color: '#FCB424'
        
    },
    [Priority.High]: {
        name: Priority.High,
        color: '#F8702B'
       
    },
    [Priority.Highest]: {
        name: Priority.Highest,
        color: '#C1470A'
       
    }
}
  



};