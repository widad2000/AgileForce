import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { SalesforceService } from 'src/app/salesforce-service.service';
@Component({
  selector: 'app-deletelist',
  templateUrl: './deletelist.component.html',
  styleUrls: ['./deletelist.component.css']
})
export class DeletelistComponent {
  Info !:any [];

  constructor( @Inject(MAT_DIALOG_DATA) public data: { IdList: any},
  public dialogRef: MatDialogRef<DeletelistComponent>,
  private salesforceService: SalesforceService
  ) {}

  ngOnInit() {
    const talkListId = this.data.IdList;
    console.log('hjfj'+talkListId);
    
    this.salesforceService.getInfos(`SELECT Id, Name FROM List__c WHERE Id='${talkListId}'`).subscribe(response => {
      this.Info = response.records;
      
      },
      error => {
        console.error('Error retrieving accounts:', error);
      }
    );
    
  }
  deleteo(IdListee : any){
    this.salesforceService.deleteRecord('List__c',IdListee).subscribe(response => {
     
      window.location.reload();
      },
      error => {
        console.error('Error retrieving accounts:', error);
      }
    );

  }
}
