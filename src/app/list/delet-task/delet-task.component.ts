import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { SalesforceService } from 'src/app/salesforce-service.service';


@Component({
  selector: 'app-delet-task',
  templateUrl: './delet-task.component.html',
  styleUrls: ['./delet-task.component.css']
})
export class DeletTaskComponent {
  Task !: any[];
  constructor( @Inject(MAT_DIALOG_DATA) public data: { IdTASK: any},
  public dialogRef: MatDialogRef<DeletTaskComponent>,
  private salesforceService: SalesforceService
  ) {}
  
  ngOnInit() {
    const talkId = this.data.IdTASK;
    console.log(talkId);
    
    this.salesforceService.getInfos(`SELECT Id, Name FROM Talk__c WHERE Id='${talkId}'`).subscribe(response => {
      this.Task = response.records;
      console.log(this.Task);
      
      },
      error => {
        console.error('Error retrieving accounts:', error);
      }
    );
    
  }

  delettask(taskId : any) {
    this.salesforceService.deleteRecord('Talk__c',taskId).subscribe(response => {
      window.location.reload();
      },
      error => {
        console.error('Error retrieving accounts:', error);
      }
    );

  }

}
