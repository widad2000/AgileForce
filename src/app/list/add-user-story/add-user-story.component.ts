import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material/dialog';
import { NotificationService } from 'src/app/notification.service';
import { SalesforceService } from 'src/app/salesforce-service.service';


@Component({
  selector: 'app-add-user-story',
  templateUrl: './add-user-story.component.html',
  styleUrls: ['./add-user-story.component.css']
})
export class AddUserStoryComponent implements OnInit {
  Urs !: any [];
  formGroup!: FormGroup;
  usersform !: FormGroup;
  issueTypesArrayWithColor = Object.values(appConstants.issueTypeListWithColor);
  PriorityArray = Object.values(appConstants.PriorityList);
  tags: Tag[] = [];
  newTagName: string = '';
  user!: any[];
  UsersTask !: any[];


  constructor( @Inject(MAT_DIALOG_DATA) public data: { IdList: any, IdProjet: any },
  public formBuilder: FormBuilder,
  public dialogRef: MatDialogRef<AddUserStoryComponent>,
  public colorPickerdialog: MatDialog,
  private salesforceService: SalesforceService,
  private notificationService: NotificationService
  ) {}

  ngOnInit() {
    const talkListId = this.data.IdList;
    const ProjetId = this.data.IdProjet;
    this.salesforceService.getInfos(`SELECT User__c , User__r.Name FROM UserTeam__c WHERE Team__c IN (SELECT Team__c FROM Project__c WHERE Id = '${ProjetId}')`).subscribe(response => {
      this.Urs = response.records;
      console.log(this.Urs);
      },
      error => {
        console.error('Error retrieving accounts:', error);
      }
    );
    this.formGroup = new FormGroup({
     
      text__c	: new FormControl(''),
      Name : new FormControl(''),
      priority__c : new FormControl(''),
      List__c : new FormControl(this.data.IdList),
      issueType__c : new FormControl(''),
      users : new FormControl([])
      
    });

    
  }
 




  onSubmit() {
    const formValues = this.formGroup.value;
    
    const taskObject = {
      text__c: formValues.text__c,
      Name: formValues.Name,
      priority__c : formValues.priority__c,
      List__c: this.data.IdList,
      issueType__c : formValues.issueType__c,
      createdAt__c : new Date()
     
      
    };
    const message = `${formValues.Name}`;
    this.notificationService.addNotification(message);

    this.UsersTask = formValues.users;
    console.log(formValues.users );
    console.log(taskObject);
    
    this.salesforceService.createRecord('Talk__c', taskObject).subscribe(
      response =>{
        console.log(response);
        
        const createdTaskId = response.id;
        console.log('taskid'+createdTaskId);
        
        this.UsersTask.forEach((userId: string)=>{
          const userTask = {
            Name:'newtaskuser1',
            Talk__c: createdTaskId,
            User__c: userId
          };
          console.log(userTask);
          
          this.salesforceService.createRecord('UserTask__c', userTask).subscribe(
            response => {
              console.log('UserTask created successfully:', response);
            },
            error => {
              console.error('Error creating UserTask:', error);
            }
          );
          this.dialogRef.close();
          this.dialogRef.afterClosed().subscribe(result => {
           
          
        })
      }
    )
    
  },
  error => {
    console.error('Error creating UserTask:', error.message);
  }
  )
 }

  
}

export interface Tag {
  name: string;
  color?: string;
}



export enum Priority {
  Low = 'Low',
  Lowest = 'Lowest',
  Medium = 'Medium',
  High = 'High',
  Highest = 'Highest'
}


export enum IssueType {
  Task = 'task',
  SubTask = 'sub-task',
  Bug = 'bug',
  Epic = 'epic',
  Story = 'story'
}

export const appConstants = {

  /** Issue Types with ttheir hardcoded colors */
  issueTypeListWithColor: {
      [IssueType.Bug]: {
          name: IssueType.Bug,
          color: '#99333352'
      },
      [IssueType.Epic]: {
          name: IssueType.Epic,
          color: '#33996652'
      },
      [IssueType.Story]: {
          name: IssueType.Story,
          color: '#fff3d4'
      },
      [IssueType.Task]: {
          name: IssueType.Task,
          color: '#99ccff61'
      },
      [IssueType.SubTask]: {
          name: IssueType.SubTask,
          color: '#3d7e9a4d'
      }
  },
  PriorityList: {
      [Priority.Low]: {
          name: Priority.Low,
          color: '#FCD432'
         
      },
      [Priority.Lowest]: {
          name: Priority.Lowest,
          color: '#FFE95D'
          
      },
      [Priority.Medium]: {
          name: Priority.Medium,
          color: '#FCB424'
          
      },
      [Priority.High]: {
          name: Priority.High,
          color: '#F8702B'
         
      },
      [Priority.Highest]: {
          name: Priority.Highest,
          color: 'C1470A'
         
      }
  }
  

};