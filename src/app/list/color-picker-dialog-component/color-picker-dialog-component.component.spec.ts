import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ColorPickerDialogComponentComponent } from './color-picker-dialog-component.component';

describe('ColorPickerDialogComponentComponent', () => {
  let component: ColorPickerDialogComponentComponent;
  let fixture: ComponentFixture<ColorPickerDialogComponentComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ColorPickerDialogComponentComponent]
    });
    fixture = TestBed.createComponent(ColorPickerDialogComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
