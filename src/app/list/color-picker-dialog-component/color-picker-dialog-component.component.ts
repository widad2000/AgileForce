import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-color-picker-dialog-component',
  templateUrl: './color-picker-dialog-component.component.html',
  styleUrls: ['./color-picker-dialog-component.component.css']
})
export class ColorPickerDialogComponentComponent implements OnInit {

  selectedColor = '';

  constructor(
    public dialogRef: MatDialogRef<ColorPickerDialogComponentComponent>,
  ) { }

  ngOnInit(): void {
  }

  onChangeComplete(c: any) {
    this.selectedColor = c.color.hex;
    console.log(c);
  }

}
