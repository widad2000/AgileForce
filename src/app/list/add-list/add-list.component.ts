import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup,FormControl, Validators } from '@angular/forms';
import { Data, Router } from '@angular/router';
import { SalesforceService } from 'src/app/salesforce-service.service';

@Component({
  selector: 'app-add-list',
  templateUrl: './add-list.component.html',
  styleUrls: ['./add-list.component.css']
})
export class AddListComponent {
  myForm!: FormGroup;
  id: any;

  constructor(
    private route :Router ,
    public dialogRef: MatDialogRef<AddListComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { IdProjet: any ,Sprint__c : any  }, 
    private formBuilder: FormBuilder,
    private SalesforceService : SalesforceService
  ) { }


  ngOnInit() {
    console.log("this.data.Sprint_c test : ",this.data.Sprint__c)
    this.myForm = new FormGroup({
     
      Name: new FormControl(''),
      Project__c : new FormControl(this.data.IdProjet),
      Sprint__c : new FormControl(this.data.Sprint__c),
      
    });
    
    
  }


  onSubmit(): void {
    
    console.log("this.data.Sprint_c test 2222 : ",this.data.Sprint__c)
    this.SalesforceService.createRecord('List__c', this.myForm.value).subscribe(
      response => {
        console.log('List created successfully:', response);
        this.dialogRef.close();
        this.dialogRef.afterClosed().subscribe(result => {
          window.location.reload();
          console.log(this.id);
        });
      },
      error => {
        console.error('Error creating list:', error);
      }
    );
  }


}
