import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SalesforceService } from '../salesforce-service.service';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { AddUserStoryComponent } from './add-user-story/add-user-story.component';
import { AddListComponent } from './add-list/add-list.component';
import { forkJoin } from 'rxjs';
import { NodeletelistComponent } from './nodeletelist/nodeletelist.component';
import { DeletelistComponent } from './deletelist/deletelist.component';
import { UpdateTaskComponent } from './update-task/update-task.component';
import { DeletTaskComponent } from './delet-task/delet-task.component';
import { CdkDragDrop, moveItemInArray, transferArrayItem, DragDropModule  } from '@angular/cdk/drag-drop';
import { ListDetailsComponent } from './list-details/list-details.component';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent {
 
  id!:any;
  projet: any;
  List: any[] = [];
  Task: any[] = [];
  IdProjet : any;
  LastList : any[] = [];
  connectedListIds: string[] = [];
  board ! : any;
  springs !: any[];
  selectedOption ! : string;
  Sprint__c ! : any;
  constructor(private SalesforceService: SalesforceService,private router :Router ,private route: ActivatedRoute,private ActivatedRoute : ActivatedRoute, private _dialog: MatDialog,private dialog: MatDialog) {
    const data = this.route.snapshot?.data?.['data'];
    if (data) {
      this.IdProjet = data.IdProjet;
  this.Sprint__c = data.Sprint__c;
      // Now you can use the 'idProjet' as needed in your ListComponent
    }

   }
  ngOnInit(): void {
    this.springs = ["All","Sprint 1","Sprint 2","Sprint 3","Sprint 4","Sprint 5","Sprint 6","Sprint 7","Sprint 8","Sprint 9","Sprint 10","Sprint 11","Sprint 12"];
    this.route.params.subscribe(params => {
      this.IdProjet = params['id'];
      
     /* this.SalesforceService.getInfos(`SELECT Id, Name FROM List__c WHERE Project__c ='${this.IdProjet}'`)
        .subscribe(
          response => {
            this.List = response.records;
            console.log(this.List);

            this.SalesforceService.getInfos(`SELECT Id, Name, CreatedAt__c, Pourcentage__c, Priority__c,done__c, issueType__c, text__c, theme__r.Name, (SELECT User__r.Id, User__r.SmallPhotoUrl, User__r.Name FROM UserTasks__r) FROM Talk__c WHERE List__r.Project__c ='${this.IdProjet}'`)
            .subscribe(
              response => {
                this.Task =  response.records; // Assuming the response is an array of Task objects.
                console.log(this.Task); // Log the fetched data to the console.
              },
              error => {
                console.error(error); // Log any errors that may occur during the API call.
              }
            );
  
      
    
            const requests = this.List.map(a =>
              this.SalesforceService.getInfos(`SELECT Id, Name, createdAt__c, pourcentage__c,done__c, priority__c, issueType__c, text__c, theme__r.Name, (SELECT User__r.Id, User__r.SmallPhotoUrl, User__r.Name FROM UserTasks__r) FROM Talk__c where List__c = '${a.Id}'`)
            );
    
            forkJoin(requests).subscribe(
              responses => {
                this.LastList = [];
                responses.forEach((response, index) => {
                  const b = {
                    Id: this.List[index].Id,
                    Name: this.List[index].Name,
                    Tasks: response.records
                  };
                  this.LastList.push(b);
                });
                console.log(this.LastList);
              },
              error => {
                console.error('Error updating talk:', error);
              }
            );
          },
          error => {
            console.error('Error retrieving accounts:', error);
          }
        );*/
        if(this.selectedOption=="All" || this.selectedOption==null ){
          console.log("ngOnInit list ",this.selectedOption);
          this.SalesforceService.getInfos(`SELECT Id, Name FROM List__c WHERE Project__c ='${this.IdProjet}'  `)
          .subscribe(
            response => {
              this.List = response.records;
              console.log(this.List);
    
              this.SalesforceService.getInfos(`SELECT Id, Name, CreatedAt__c, Pourcentage__c, Priority__c,done__c, issueType__c, text__c, theme__r.Name, (SELECT User__r.Id, User__r.SmallPhotoUrl, User__r.Name FROM UserTasks_r) FROM Talk__c WHERE Listr.Project__c ='${this.IdProjet}'`)
              .subscribe(
                response => {
                  this.Task =  response.records; // Assuming the response is an array of Task objects.
                  console.log(this.Task); // Log the fetched data to the console.
                },
                error => {
                  console.error(error); // Log any errors that may occur during the API call.
                }
              );
    
        
      
              const requests = this.List.map(a =>
                this.SalesforceService.getInfos(`SELECT Id, Name, createdAt__c, pourcentage__c,done__c, priority__c, issueType__c, text__c, theme__r.Name, (SELECT User__r.Id, User__r.SmallPhotoUrl, User__r.Name FROM UserTasks__r) FROM Talk__c where List__c = '${a.Id}'`)
              );
      
              forkJoin(requests).subscribe(
                responses => {
                  this.LastList = [];
                  responses.forEach((response, index) => {
                    const b = {
                      Id: this.List[index].Id,
                      Name: this.List[index].Name,
                      Tasks: response.records
                    };
                    this.LastList.push(b);
                  });
                  console.log(this.LastList);
                },
                error => {
                  console.error('Error updating talk:', error);
                }
              );
            },
            error => {
              console.error('Error retrieving accounts:', error);
            }
          );
        }else{
          console.log("ngOnInit list ",this.selectedOption);
          this.SalesforceService.getInfos(`SELECT Id, Name FROM List__c WHERE Project__c ='${this.IdProjet}' and Sprint__c='${this.selectedOption}' `)
          .subscribe(
            response => {
              this.List = response.records;
              console.log('my list',this.List);
              
              if(this.List.length!=0){
                this.SalesforceService.getInfos(`SELECT Id, Name, CreatedAt__c, Pourcentage__c, Priority__c,done__c, issueType__c, text__c, theme__r.Name, (SELECT User__r.Id, User__r.SmallPhotoUrl, User__r.Name FROM UserTasks__r) FROM Talk__c WHERE List__r.Project__c ='${this.IdProjet}'`)
                .subscribe(
                  response => {
                    this.Task =  response.records; // Assuming the response is an array of Task objects.
                    console.log(this.Task); // Log the fetched data to the console.
                  },
                  error => {
                    console.error(error); // Log any errors that may occur during the API call.
                  }
                );
        
          
        
                const requests = this.List.map(a =>
                  this.SalesforceService.getInfos(`SELECT Id, Name, createdAt__c, pourcentage__c,done__c, priority__c, issueType__c, text__c, theme__r.Name, (SELECT User__r.Id, User__r.SmallPhotoUrl, User__r.Name FROM UserTasks__r) FROM Talk__c where List__c = '${a.Id}'`)
                );
        
                forkJoin(requests).subscribe(
                  responses => {
                    this.LastList = [];
                    responses.forEach((response, index) => {
                      const b = {
                        Id: this.List[index].Id,
                        Name: this.List[index].Name,
                        Tasks: response.records
                      };
                      this.LastList.push(b);
                    });
                    console.log(this.LastList);
                  },
                  error => {
                    console.error('Error updating talk:', error);
                  }
                );
              }else{
               
                this.LastList = [];
    
              }
              console.log(this.List);
      
           
            },
            error => {
              console.error('Error retrieving accounts:', error);
            }
          );
        }
    });
    
    this.connectedListIds = this.LastList.map(list => 'list-' + list.Name);
  
  }

  details(talk: any, list: any){
    this._dialog.open( ListDetailsComponent, { data: {talk : talk, list : list}, width: '500px' }).afterClosed()
      .subscribe(res => {
       
      });
  }

///////////
   onOptionSelected(spring : any) {
    this.selectedOption=spring;
    console.log('Selected option:', spring);
    if(spring=="All"){
      this.SalesforceService.getInfos(`SELECT Id, Name FROM List__c WHERE Project__c ='${this.IdProjet}'  `)
      .subscribe(
        response => {
          this.List = response.records;
          console.log(this.List);

          this.SalesforceService.getInfos(`SELECT Id, Name, CreatedAt__c, Pourcentage__c, Priority__c,done__c, issueType__c, text__c, theme__r.Name, (SELECT User__r.Id, User__r.SmallPhotoUrl, User__r.Name FROM UserTasks__r) FROM Talk__c WHERE Listr.Project__c ='${this.IdProjet}'`)
          .subscribe(
            response => {
              this.Task =  response.records; // Assuming the response is an array of Task objects.
              console.log(this.Task); // Log the fetched data to the console.
            },
            error => {
              console.error(error); // Log any errors that may occur during the API call.
            }
          );

    
  
          const requests = this.List.map(a =>
            this.SalesforceService.getInfos(`SELECT Id, Name, createdAt__c, pourcentage__c,done__c, priority__c, issueType__c, text__c, theme__r.Name, (SELECT User__r.Id, User__r.SmallPhotoUrl, User__r.Name FROM UserTasks__r) FROM Talk__c where List__c = '${a.Id}'`)
          );
  
          forkJoin(requests).subscribe(
            responses => {
              this.LastList = [];
              responses.forEach((response, index) => {
                const b = {
                  Id: this.List[index].Id,
                  Name: this.List[index].Name,
                  Tasks: response.records
                };
                this.LastList.push(b);
              });
              console.log(this.LastList);
            },
            error => {
              console.error('Error updating talk:', error);
            }
          );
        },
        error => {
          console.error('Error retrieving accounts:', error);
        }
      );
    }else{
      this.SalesforceService.getInfos(`SELECT Id, Name FROM List__c WHERE Project__c ='${this.IdProjet}' and Sprint__c='${spring}' `)
      .subscribe(
        response => {
          this.List = response.records;
          if(this.List.length!=0){
            this.SalesforceService.getInfos(`SELECT Id, Name, CreatedAt__c, Pourcentage__c, Priority__c,done__c, issueType__c, text__c, theme__r.Name, (SELECT User__r.Id, User__r.SmallPhotoUrl, User__r.Name FROM UserTasks__r) FROM Talk__c WHERE List__r.Project__c ='${this.IdProjet}'`)
            .subscribe(
              response => {
                this.Task =  response.records; // Assuming the response is an array of Task objects.
                console.log(this.Task); // Log the fetched data to the console.
              },
              error => {
                console.error(error); // Log any errors that may occur during the API call.
              }
            );
    
      
    
            const requests = this.List.map(a =>
              this.SalesforceService.getInfos(`SELECT Id, Name, createdAt__c, pourcentage__c,done__c, priority__c, issueType__c, text__c, theme__r.Name, (SELECT User__r.Id, User__r.SmallPhotoUrl, User__r.Name FROM UserTasks__r) FROM Talk__c where List__c = '${a.Id}'`)
            );
    
            forkJoin(requests).subscribe(
              responses => {
                this.LastList = [];
                responses.forEach((response, index) => {
                  const b = {
                    Id: this.List[index].Id,
                    Name: this.List[index].Name,
                    Tasks: response.records
                  };
                  this.LastList.push(b);
                });
                console.log(this.LastList);
              },
              error => {
                console.error('Error updating talk:', error);
              }
            );
          }else{
           
            this.LastList = [];

          }
          console.log(this.List);
  
       
        },
        error => {
          console.error('Error retrieving accounts:', error);
        }
      );

    }
  


  }
  //////////

  onDragStart(event: DragEvent, item: string) {
    event.dataTransfer?.setData('text',item);
  }

  onDragEnd(event: DragEvent) {
          
  }

  onDragOver(event: DragEvent) {
    event.preventDefault();
  }

  onDrop(event: DragEvent,newListName: string) {
    event.preventDefault();
    const data = event.dataTransfer?.getData('text');
    if (data) {
      console.log(data, newListName);
      this.updateTalkRecord(data,{ List__c: newListName });
      
    }}

   



    updateTalkRecord(a: any, b : any) {
      this.SalesforceService.updateRecord('Talk__c', a, b).subscribe(
        response => {
          console.log('Update successful:', response);
        },
        error => {
          console.error('Error updating talk:', error);
        }
      );
    }

    exportList(name: any,id :any){

  
      this.SalesforceService.getBoard("ZltSxNUb").subscribe(
        (response) => {
          // Handle the response if needed
          console.log('List created successfully:', response);
          this.board=response;
          this.SalesforceService.createList(name,this.board.id).subscribe(
            (response) => {
              // Handle the response if needed
              console.log('List created successfully:', response);
            },
            (error) => {
              // Handle errors if any
              console.error('Error creating list:', error);
            }
          );
        },
        (error) => {
          // Handle errors if any
          console.error('Error creating list:', error);
        }
      );


    }
    exportListWithCards(name: any,Task: any[] ){

  
      this.SalesforceService.getBoard("ZltSxNUb").subscribe(
        (response) => {
          // Handle the response if needed
          console.log('List created successfully:', response);
          this.board=response;
          this.SalesforceService.createList(name,this.board.id).subscribe(
            (response) => {
              this.board=response;
              // Handle the response if needed
              console.log('List created successfully:', response);
              Task.forEach(element => {
  console.log("hadaaa howaa element : ",element);

                this.SalesforceService.createCard(element.Name,this.board.id).subscribe(
                  (response) => {
                  
                    console.log('Card created successfully:', response);
      
                  } ,
                  (error) => {
                    // Handle errors if any
                    console.log(' listId successfully:', element.talk.id);
      
                    console.error('Error creating card:', error);
      
                  }
                  
                  
                  
                  
                  );






              });
              






            },
            (error) => {
              // Handle errors if any
              console.error('Error creating list:', error);
            }
          );
        },
        (error) => {
          // Handle errors if any
          console.error('Error creating list:', error);
        }
      );


    }

    exportCard(NameList : any,nameCard : any){
     /* this.SalesforceService.getBoard("ZltSxNUb").subscribe(
        (response) => {
          // Handle the response if needed
          console.log('List created successfully:', response);
          this.board=response;
          this.SalesforceService.createList(name,this.board.id).subscribe(
            (response) => {
              // Handle the response if needed
              console.log('List created successfully:', response);
            },
            (error) => {
              // Handle errors if any
              console.error('Error creating list:', error);
            }
          );
        },
        (error) => {
          // Handle errors if any
          console.error('Error creating list:', error);
        }
      );
  
      this.SalesforceService.createCard(name,desc,idList).subscribe(
        (response) => {
          // Handle the response if needed
          console.log('List created successfully:', response);
          this.board=response;
          this.SalesforceService.createList(name,this.board.id).subscribe(
            (response) => {
              // Handle the response if needed
              console.log('List created successfully:', response);
            },
            (error) => {
              // Handle errors if any
              console.error('Error creating list:', error);
            }
          );
        },
        (error) => {
          // Handle errors if any
          console.error('Error creating list:', error);
        }
      );
*/
/*this.SalesforceService.getList(idList).subscribe(
  (listData: any) => {
    // The listData object will contain the details of the list, including its ID
    const listId = listData.id;
    console.log('List ID:', listId);
  },
  (error) => {
    console.error('Error fetching list details:', error);
  }
);*/

this.SalesforceService.getBoard("ZltSxNUb").subscribe(
  (response) => {
    // Handle the response if needed
    console.log('List created successfully:', response);
    this.board=response;
    this.SalesforceService.getLists(this.board.id,NameList).subscribe(
      (lists: any) => { // Use 'any[]' type for the parameter
        // The lists array will contain information about all lists on the board
        // Find the list you want and get its ID
        const myList = lists.find((list: { name: string; }) => list.name === NameList);
        if (myList) {
          const listId = myList.id;
          console.log('List ID:', listId);

          this.SalesforceService.createCard(nameCard,listId).subscribe(
            (response) => {
            
              console.log('Card created successfully:', response);

            } ,
            (error) => {
              // Handle errors if any
              console.log(' listId successfully:', listId);

              console.error('Error creating card:', error);

            }
            
            
            
            
            );
        } else {
          console.log('List not found.');
        }
      },
      (error) => {
        console.error('Error fetching lists:', error);
      }
    );
  },
  (error) => {
    // Handle errors if any
    console.error('Error creating list:', error);
  }
);


    }




    addUserStrory(IdList: any){
      this._dialog.open( AddUserStoryComponent, { data: {IdList: IdList, IdProjet: this.IdProjet}, width: '500px' }).afterClosed()
      .subscribe(res => {
       
      });
    }
    deletList(IdList : any, task :any[]){
     
      if(task.length != 0){
        this._dialog.open( NodeletelistComponent, { data: {IdList: IdList}, width: '500px' }).afterClosed()
      .subscribe(res => {
       
      });

      }
      else {
      this._dialog.open( DeletelistComponent, { data: {IdList: IdList}, width: '500px' }).afterClosed()
      .subscribe(res => {
       
      });

      }
     
    }

    /*addList(){
      this._dialog.open( AddListComponent, { data: {IdProjet: this.IdProjet}, width: '500px' }).afterClosed()
      .subscribe(res => {
       
      });
    }*/
    addList(){
      this.dialog.open( AddListComponent, { data: {IdProjet: this.IdProjet,Sprint__c : this.selectedOption}, width: '500px' }).afterClosed()
      .subscribe(res => {
       
      });
    }
    deletetask(IdTASK: any){
      console.log(IdTASK);
      this._dialog.open( DeletTaskComponent, { data: {IdTASK: IdTASK}, width: '500px' }).afterClosed()
      .subscribe(res => {
       
      });
   }
   editeTask(IdTask: any, listId : any){
     console.log('thisis'+IdTask);
     this._dialog.open( UpdateTaskComponent, { data: {IdTask: IdTask, projetId : this.IdProjet, listId: listId}, width: '500px' }).afterClosed()
      .subscribe(res => {
      });
    
      
      
  }

    showButtonsFlag: boolean = false;  
    getColor(priority: string) {
      let color: string;
      switch (priority) {
      case 'High':
      color = '#F8702B';
      break;
      case 'Medium':
      color = '#FCB424';
      break;
      case 'Low':
      color = '#FCD432';
      break;
      case 'Lowest':
      color = '#FFE95D';
      break;
      case 'Highest':
      color = '#C1470A';
      break;
      default:
      color = '';
      break;
      }
      return color;
      }
      getPourcentage(priority: string) {
        let size: string;
        switch (priority) {
        case 'High':
          size = '80%';
        break;
        case 'Medium':
          size = '50%';
        break;
        case 'Low':
          size = '30%';
        break;
        case 'Lowest':
          size = '10%';
        break;
        case 'Highest':
          size = '100%';
        break;
        default:
          size = '';
        break;
        }
        return size;
        }

        //-----------------------------------------------------------------------

  drop(event: CdkDragDrop<any[]>, idlist : any) {
    
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);

        const IdTask = event.container.data.map(item => item.Id);

        IdTask.forEach(item => {
          this.updateTalkRecord(item, { List__c: idlist });
        });
       
    }
    
  }
    
}