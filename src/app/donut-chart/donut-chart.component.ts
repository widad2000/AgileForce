import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ChartData, ChartOptions, ChartType, ChartDataset } from 'chart.js';
import { SalesforceService } from '../salesforce-service.service';
import {  CategoryScale, LinearScale, LogarithmicScale } from 'chart.js';
import { Chart } from 'chart.js';
import { registerables } from 'chart.js';


@Component({
  selector: 'app-donut-chart',
  templateUrl: './donut-chart.component.html',
  styleUrls: ['./donut-chart.component.css']
})
export class DonutChartComponent implements OnInit {
  private chart!: Chart;
  nombretasks !: any;
  public accounts!: any[];
  numberOfBlockedTasks !: any[];
  num : number =0;
  numberOfProjects !: any[];
  numberOfUsers: number = 0;
  numberOfTeams: number = 0;
  public datatoins!: any[];
  @ViewChild('chartCanvas') chartCanvas!: ElementRef;
  @ViewChild('chartdogCanvas') chartdogCanvas!: ElementRef;
  delai: Time = { days: 0, hours: 0, minutes: 0 };
  IdAccount!: string;
  public filteredAccounts!: any[];
  timer: any;
  endDate !: Date;
  ProjectInfo !: any[];
  numberOfUserss!: any[];
  numuser : number =0;
  event !: any [];
  userstaskdone !: any [];
  pourcentage !: any;

  constructor(private salesforceService: SalesforceService) {}
  public chartOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false
  };
  public chartLabels: string[] = [];
  public chartData: ChartData<'doughnut', number[], string> = {
    labels: this.chartLabels,
    datasets: [
      {
        data: [],
        backgroundColor: ['rgba(245, 105, 39, 0.8)', 'rgba(245, 161, 39, 0.9)', 'rgba(245, 229, 39, 0.9)', 'rgba(255, 255, 0, 0.77)', 'rgba(249, 77, 9, 0.6)'],
        hoverBackgroundColor: ['rgba(255, 81, 0, 0.98)', 'rgba(255, 115, 0, 0.98)', 'rgba(245, 212, 0, 1)', 'rgba(234, 229, 0, 1)','rgba(250, 74, 10, 0.56)'],

      }
    ]
  };



  ngOnInit() {
   
    this.salesforceService.getInfoUserLog().subscribe(
      response => {
        this.IdAccount = response.user_id;
        console.log(this.IdAccount);

        this.salesforceService.getInfos(`SELECT Id , Name, theme__r.Name, Description__c, Team__r.Name FROM Project__c`)
          .subscribe(
            response => {
              this.accounts = response.records;
              this.filteredAccounts = this.accounts;
              console.log(this.accounts);
              const initialProjectId = this.accounts[0].Id;
              if (initialProjectId) {
                this.onProjectSelected({ target: { value: initialProjectId } });
              }
            },
            error => {
              console.error('Error retrieving accounts:', error);
            }
          );
      },
      error => {
        console.error('Error retrieving user info:', error);
      }
    );
    this.salesforceService.getInfoUserLog().subscribe(
      response => {
        this.IdAccount = response.user_id;
        console.log(this.IdAccount);

        this.salesforceService.getInfos(`SELECT Id , Name, theme__r.Name, Description__c, Team__r.Name FROM Project__c`)
          .subscribe(
            response => {
              this.accounts = response.records;
              this.filteredAccounts = this.accounts;
              console.log(this.accounts);

              // Create the chart
              this.createChart();
            },
            error => {
              console.error('Error retrieving accounts:', error);
            }
          );
      },
      error => {
        console.error('Error retrieving user info:', error);
      }
    );
      
  }

  users : User[] = [];

  getProgressWidth(user: User): string {
    const progressPercentage = (user.tasksCompleted ) * 10;
    this.pourcentage=progressPercentage;
    return `${progressPercentage}%`;
  }

  onProjectSelected(selectedProjectId: any) {
    const projectId = selectedProjectId.target.value;
    this.users=[];

    this.salesforceService.getInfos(`SELECT User__r.Name , Count(Talk__r.Name) from UserTask__c where Talk__r.List__r.Project__c = '${projectId}' and Talk__r.done__c = true GROUP BY User__r.Name`).subscribe(
      res=>{
        this.userstaskdone=res.records;
        this.userstaskdone.forEach(u=>{
          const a ={
            name : u.Name,
            tasksCompleted : u.expr0,
          };
          this.users.push(a);
        })
      }
    );
   

    this.salesforceService.getInfos(`SELECT count(Id) from Talk__c where issueType__c = 'bug' and  List__r.Project__c='${projectId}'`)
        .subscribe(
          response => {
            this.numberOfBlockedTasks = response.records;
            this.numberOfBlockedTasks.forEach(element => {
              this.num = element.expr0;
            });
           
            
          },
          error => {
            console.error('Error retrieving accounts:', error);
          }
        );

    
      this.salesforceService.getInfos(`SELECT Subject, Description,StartDateTime, EndDate from Event where WhatId = '${projectId}'`).subscribe(
        res=>{
          this.event = res.records;
        }
      );

        this.salesforceService.getInfos(`SELECT Name , StartDate__c, EndDate__c from Project__c where Id ='${projectId}'`)
        .subscribe(
          response => {
            
            const startDateStr = response.records[0].StartDate__c;
            const endDateStr = response.records[0].EndDate__c;
            const startDate = new Date(startDateStr);
            const endDate = new Date(endDateStr);
            const timeDiff = endDate.getTime() - startDate.getTime();
            this.delai = this.millisecondsToTime(timeDiff);
            console.log(this.delai);

            this.endDate = endDate;
            this.startTimer();


          },
          error => {
            console.error('Error retrieving accounts:', error);
          }
        );

        this.salesforceService.getInfos(`SELECT count(User__c) from UserTeam__c where Team__c IN (Select Team__c from Project__c where Id ='${projectId}')`)
        .subscribe(
          response => {
            this.numberOfUserss = response.records;
            this.numberOfUserss.forEach(element => {
              this.numuser = element.expr0;
            });
           
            
          },
          error => {
            console.error('Error retrieving accounts:', error);
          }
        );
        console.log(projectId);
        this.salesforceService.getInfos(`SELECT Name FROM List__c WHERE Project__c = '${projectId}'`).subscribe(
          response => {
            this.chartLabels = response.records.map((record: any) => record.Name);
            console.log(this.chartLabels);
      
            let totalTasks = 0;
            const dataPromises: Promise<any>[] = [];
      
            this.chartLabels.forEach((label, index) => {
              const dataPromise = this.salesforceService.getInfos(`SELECT COUNT(Id) FROM Talk__c WHERE List__c IN (SELECT Id FROM List__c WHERE Project__c = '${projectId}' AND Name = '${label}')`).toPromise();
              dataPromises.push(dataPromise);
            });
      
            Promise.all(dataPromises)
              .then(responses => {
                responses.forEach((response, index) => {
                  const taskCount = response.records[0].expr0;
                  totalTasks += taskCount;
                  const percentage = (taskCount / totalTasks) * 100;
                  this.chartData.datasets[0].data[index] = response.records[0].expr0;
                });
                this.createdogChart();
      
                // Update the chart data
                this.chart.data.labels = this.chartLabels;
                this.chart.update();
              })
              .catch(error => {
                console.error('Error retrieving task counts:', error);
              });
          },
          error => {
            console.error('Error retrieving list names:', error);
          }
        );

       }
       onProjectS(selectedProjectId: any) {
        const projectId = selectedProjectId.target.value;
        console.log(projectId);
        this.salesforceService.getInfos(`SELECT Name FROM List__c WHERE Project__c = '${projectId}'`).subscribe(
          response => {
            this.chartLabels = response.records.map((record: any) => record.Name);
            console.log(this.chartLabels);
      
            let totalTasks = 0;
            const dataPromises: Promise<any>[] = [];
      
            this.chartLabels.forEach((label, index) => {
              const dataPromise = this.salesforceService.getInfos(`SELECT COUNT(Id) FROM Talk__c WHERE List__c IN (SELECT Id FROM List__c WHERE Project__c = '${projectId}' AND Name = '${label}')`).toPromise();
              dataPromises.push(dataPromise);
            });
      
            Promise.all(dataPromises)
              .then(responses => {
                responses.forEach((response, index) => {
                  const taskCount = response.records[0].expr0;
                  totalTasks += taskCount;
                  const percentage = (taskCount / totalTasks) * 100;
                  this.chartData.datasets[0].data[index] = response.records[0].expr0;
                });
                this.createdogChart();
      
                // Update the chart data
                this.chart.data.labels = this.chartLabels;
                this.chart.update();
              })
              .catch(error => {
                console.error('Error retrieving task counts:', error);
              });
          },
          error => {
            console.error('Error retrieving list names:', error);
          }
        );
      }
      createdogChart() {
        const chartdogCanvas = this.chartdogCanvas.nativeElement.getContext('2d');
      
        
      // Destroy the existing chart if it exists
      if (this.chart) {
        this.chart.destroy();
      }
    
      // Create the new chart
      this.chart = new Chart(chartdogCanvas, {
        type: 'doughnut',
        data: this.chartData,
        options: {
          ...this.chartOptions,
          
          plugins: {
            tooltip: {
              callbacks: {
                label: (context) => {
                  const labelIndex = context.dataIndex;
                  const listName = this.chartLabels[labelIndex];
                  const taskCount = this.chartData.datasets[0].data[labelIndex];
                  return `${listName}: ${taskCount} Tasks`;
                }
              }
            }
          }
        }
      });
    }
       
  createChart() {
    
    

    // Fetch the data and perform necessary calculations
    const chartCanvas = this.chartCanvas.nativeElement.getContext('2d');

// Fetch the data and perform necessary calculations
this.salesforceService.getInfos(`SELECT User__r.Name, COUNT(Id) FROM UserTeam__c WHERE Team__c IN (SELECT Team__c FROM Project__c WHERE Team__c != NULL) GROUP BY User__r.Name`)
  .subscribe(
    response => {
      const colors = ['rgba(245, 105, 39, 0.8)', 'rgba(245, 161, 39, 0.9)', 'rgba(245, 229, 39, 0.9)', 'rgba(253, 255, 0, 1)', 'rgba(153, 102, 255, 0.6)'];
      const userProjects = response.records;
      console.log('data: ', userProjects);

      // Extracting required data for chart
      const userNames = userProjects.map((userProject: any) => userProject.Name ?? '');
      const projectCounts = userProjects.map((userProject: any) => userProject.expr0);
      console.log('hello: ', projectCounts);

      // Create the chart using Chart.js
      new Chart(chartCanvas, {
        type: 'bar',
        data: {
          labels: userNames,
          datasets: [{
            data: projectCounts,
            backgroundColor: colors.slice(0, userNames.length),
            borderColor: 'rgba(0, 0, 0, 0)',
            borderWidth: 10
          }]
        },
        options: {
          responsive: true,
          maintainAspectRatio: false,
          scales: {
            y: {
              grid: {
                display: false
              },
              beginAtZero: true,
              type: 'linear',
              title: {
                display: false,
                text: 'Project',
                font: {
                  size: 16,
                  weight: 'bold'
                }
              }
            },
            x: {
              grid: {
                display: false
              },
              title: {
                display: false,
                text: 'User',
                font: {
                  size: 16,
                  weight: 'bold'
                }
              }
            }
          },
          plugins: {
            legend: {
              display: false,
              position: 'top',
              title: {
                display: true,
                text: 'Number of Projects by User',
                font: {
                  size: 18,
                }
              }
            }
          }
        }
      });

      console.log(userProjects);
    },
    error => {
      console.error('Error retrieving user projects:', error);
    }
  );


  }
        millisecondsToTime(milliseconds: number): Time {
          const days = Math.floor(milliseconds / (1000 * 60 * 60 * 24));
          const hours = Math.floor((milliseconds % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
          const minutes = Math.floor((milliseconds    % (1000 * 60 * 60)) / (1000 * 60));
          const timeObj = { days: days, hours: hours, minutes: minutes };
          return timeObj;
        }

        startTimer(): void {
          this.timer = setInterval(() => {
            this.updateTimerDisplay();
          }, 1000);
        }
      
        stopTimer(): void {
          clearInterval(this.timer);
        }
      
        updateTimerDisplay(): void {
          const currentTime = new Date();
          const remainingTime = Math.max(this.endDate.getTime() - currentTime.getTime(), 0);
      
          const days = Math.floor(remainingTime / (1000 * 60 * 60 * 24));
          const hours = Math.floor((remainingTime % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
          const minutes = Math.floor((remainingTime % (1000 * 60 * 60)) / (1000 * 60));
          const seconds = Math.floor((remainingTime % (1000 * 60)) / 1000).toString().padStart(2, '0');

          const timerDisplay = document.getElementById('timerDisplay');
          if (timerDisplay) {
            timerDisplay.textContent = `${days} Days ${hours} h ${minutes} min`;
          }
      
          // Arrêter le chronomètre lorsque le temps restant est inférieur ou égal à zéro
          if (remainingTime <= 0) {
            this.stopTimer();
      
            // Ajoutez ici le code pour afficher un message ou effectuer une action spécifique
            console.log('Temps écoulé !');
          }
        }
        

  
}

interface Time {
  days: number;
  hours: number;
  minutes: number;
}

interface User {
  name: string;
  tasksCompleted: number;
}