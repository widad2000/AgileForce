import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { MatDialogModule } from '@angular/material/dialog';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddprojectComponent } from './addproject/addproject.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SidebarComponent } from './sidebar/sidebar.component';
import { HeaderComponent } from './header/header.component';
import { TeamListComponent } from './team-list/team-list.component';
import { ProjetComponent } from './projet/projet.component';
import { UsersteamComponent } from './usersteam/usersteam.component';
import { AddTeamComponent } from './add-team/add-team.component';
import { MatSelectModule } from '@angular/material/select';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatChipsModule} from '@angular/material/chips';
import { AdduserComponent } from './adduser/adduser.component';
import { Location } from '@angular/common';
import { CommonModule } from '@angular/common';
import { NgChartsModule } from 'ng2-charts';
import { MatStepperModule } from '@angular/material/stepper';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { DonutChartComponent } from './donut-chart/donut-chart.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { ProjectinfoComponent } from './projectinfo/projectinfo.component';
import { CalendarComponent } from './calendar/calendar.component';
import { FullCalendarModule } from '@fullcalendar/angular';
import dayGridPlugin from '@fullcalendar/daygrid'; 
import { MatIconModule } from '@angular/material/icon';
import interactionPlugin from '@fullcalendar/interaction';
import { AddEventDialogComponent } from './add-event-dialog/add-event-dialog.component';
import { ListComponent } from './list/list.component';
import { AddUserStoryComponent } from './list/add-user-story/add-user-story.component';
import { ColorPickerDialogComponentComponent } from './list/color-picker-dialog-component/color-picker-dialog-component.component';
import { AddListComponent } from './list/add-list/add-list.component';
import { NavbarComponent } from './navbar/navbar.component'
import { DeletTaskComponent } from './list/delet-task/delet-task.component';
import { UpdateTaskComponent } from './list/update-task/update-task.component';
import { DeletelistComponent } from './list/deletelist/deletelist.component';
import { NodeletelistComponent } from './list/nodeletelist/nodeletelist.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { ListDetailsComponent } from './list/list-details/list-details.component';

@NgModule({
  declarations: [
    AppComponent,
    AddprojectComponent,
    SidebarComponent,
    HeaderComponent,
    TeamListComponent,
    ProjetComponent,
    UsersteamComponent,
    AddTeamComponent,
    AdduserComponent,
    DonutChartComponent,
    UserProfileComponent,
    ProjectinfoComponent,
    CalendarComponent,
    AddEventDialogComponent,
    ListComponent,
    AddUserStoryComponent,
    ColorPickerDialogComponentComponent,
    AddListComponent,
    NavbarComponent,
    AddListComponent,
    DeletTaskComponent,
    DeletelistComponent,
    NodeletelistComponent,
    UpdateTaskComponent,
    ListDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatChipsModule,
    HttpClientModule,
    MatSnackBarModule,
    MatDialogModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    BrowserAnimationsModule,
    CommonModule,
    MatSnackBarModule,
    NgChartsModule,
    MatStepperModule,
    MatButtonModule,
    MatInputModule,
    FullCalendarModule,
    MatIconModule,
    DragDropModule,
    MatAutocompleteModule,

  ],
  providers: [Location],
  bootstrap: [AppComponent]
})
export class AppModule { }
