import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { SalesforceService } from '../salesforce-service.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
@Component({
  selector: 'app-add-team',
  templateUrl: './add-team.component.html',
  styleUrls: ['./add-team.component.css']
})
export class AddTeamComponent {
  isSubmitting: boolean = false;
  record: any = {};
  myForm!: FormGroup; 
  myForm1!: FormGroup; 
  public Urs!: any[];
  selectedOption!: string;

  constructor(public dialogRef: MatDialogRef<AddTeamComponent>, private salesforceService: SalesforceService) {}

  ngOnInit() {
    this.myForm = new FormGroup({
      Name: new FormControl('', Validators.required),
      Description__c: new FormControl(''),
      
    });
    this.myForm1 = new FormGroup({
     
      User__c: new FormControl(''),
      
    });

    this.salesforceService.getInfos(`select Id, Name from User`)
          .subscribe(response => {
            this.Urs = response.records.map((team: any) => ({
              id: team.Id,
              name: team.Name
            }));
            },
            error => {
              console.error('Error retrieving accounts:', error);
            }
          );
  }

  submitForm() {
    if (this.myForm.valid) {
      const formData = this.myForm.value;
      this.isSubmitting = true;
  
      this.salesforceService.createRecord('Team__c', formData)
        .subscribe(
          response => {
            console.log('Team added successfully:', response.id);
  
            // Get the selected users from the form
            const selectedUsers = this.myForm1.value.User__c ;
            console.log('fhhfh'+selectedUsers)
  
            if (selectedUsers.length > 0) {
              // Insert the UserTeam__c records for each selected user
              selectedUsers.forEach((userId: string) => {
                const userTeamRecord = {
                  User__c: userId,
                  Team__c: response.id
                };
  
                // Insert the UserTeam__c record
                this.salesforceService.createRecord('UserTeam__c', userTeamRecord)
                  .subscribe(
                    () => {
                      console.log('UserTeam__c record added successfully');
                      this.isSubmitting = false;
                      this.dialogRef.close();
                    },
                    error => {
                      console.error('Error adding UserTeam__c record:', error);
                      this.isSubmitting = false;
                    }
                  );
              });
              console.log('hfbnjdnjd'+selectedUsers)
              window.location.reload();
            } else {
              // No users selected, close the dialog
              this.isSubmitting = false;
              this.dialogRef.close();
            }
           
          },
          error => {
            console.error('Error adding team:', error);
            this.isSubmitting = false;
          }
        );
    }
  }
  
  
  
  
  closeDialog() {
    this.dialogRef.close();
  }
  
  selectedOptions: number[] = [];

  showSelectedOptions(ts: any) {
    
    console.log(ts);
    console.log(this.myForm.value);
  }
}
