import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TeamListComponent } from './team-list/team-list.component';
import { ProjetComponent } from './projet/projet.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { DonutChartComponent } from './donut-chart/donut-chart.component';
import { CalendarComponent } from './calendar/calendar.component';
import {ListComponent} from './list/list.component';

const routes: Routes = [
  { path: '', redirectTo: '/projet', pathMatch: 'full' },
  { path: 'team', component: TeamListComponent },
  { path: 'dashboard', component: DonutChartComponent },
  { path: 'calendar', component: CalendarComponent },
  { path: 'projet', component: ProjetComponent },
  { path :'Profil', component: UserProfileComponent },
  { path :'list/:id', component: ListComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
