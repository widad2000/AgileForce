import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { SalesforceService } from '../salesforce-service.service';
import { validateEvents } from 'angular-calendar/modules/common/util/util';
@Component({
  selector: 'app-add-event-dialog',
  templateUrl: './add-event-dialog.component.html',
  styleUrls: ['./add-event-dialog.component.css']
})
export class AddEventDialogComponent {
  eventForm!: FormGroup;
  listUsers !: any[];
  listProject !: any[];
  checklistevent !: any [];
  picklistValues: string[] = [];

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private salesforce : SalesforceService,
    public formBuilder: FormBuilder,
    public dialogRef: MatDialogRef<AddEventDialogComponent>,
  ) {}

  ngOnInit(){
    this.salesforce.description('Event').subscribe(response=>{
      const subjectField = response.fields.find((field: any) => field.name === 'Subject');
      if (subjectField && subjectField.picklistValues) {
        this.picklistValues = subjectField.picklistValues.map((value: any) => value.value);
      }
    });

    this.salesforce.getInfos('select Id, Name from project__c').subscribe(res=>{
      this.listProject=res.records;
    });

    this.eventForm =this.formBuilder.group({
      WhatId: ['', Validators.required],
      OwnerId : ['', Validators.required],
      Description : ['', Validators.required],
      Subject : ['', Validators.required],
      eventDate: ['', Validators.required],
      startTime: ['', Validators.required],
      endTime: ['', Validators.required]
    });
    console.log(this.eventForm);
    
  }
  onProjectSelected(selectedProjectId : any){
    const a = selectedProjectId.target.value
    this.salesforce.getInfos(`SELECT User__r.Name, User__c FROM UserTask__c WHERE Talk__r.List__r.Project__c = '${a}' Group by User__r.Name, User__c`).subscribe(resp=>{
      this.listUsers = resp.records;        
    });  
    
      
  }

  onSubmit(){
    
    const mydata = {
        WhatId: this.eventForm.value.WhatId,
        OwnerId : this.eventForm.value.OwnerId,
        Description :this.eventForm.value.Description,
        Subject: this.eventForm.value.Subject, 
        StartDateTime: this.combineDateTime(this.eventForm.value.eventDate, this.eventForm.value.startTime),
        EndDateTime: this.combineDateTime(this.eventForm.value.eventDate, this.eventForm.value.endTime),
    }

   
    this.salesforce.createRecord('Event',mydata).subscribe(res=>{
      console.log(res);
    });
    this.dialogRef.close();
    this.dialogRef.afterClosed().subscribe(result => {
      window.location.reload();
    
  });
      

  }
  combineDateTime(date: string, time: string): string {
    return new Date(`${date}T${time}`).toISOString();
  }

}
