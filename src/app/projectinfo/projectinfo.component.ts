import { Component, ElementRef, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SalesforceService } from '../salesforce-service.service';
import {  CategoryScale, ChartData, ChartOptions, ChartType, LinearScale, LogarithmicScale } from 'chart.js';
import { Chart } from 'chart.js';
import { registerables } from 'chart.js';

// Register the necessary chart type controller
Chart.register(...registerables);


// Register the necessary scale plugins
Chart.register(CategoryScale, LinearScale, LogarithmicScale);

@Component({
  selector: 'app-projectinfo',
  templateUrl: './projectinfo.component.html',
  styleUrls: ['./projectinfo.component.css']
})
export class ProjectinfoComponent {
  public accounts!: any[];
  public Users!: any[];
  public filteredAccounts!: any[];
  private chart!: Chart;
  @ViewChild('chartdogCanvas') chartdogCanvas!: ElementRef;
  Name: any;
  Team: any;
  Description__c: any;
  StartDate: any;
  EndDate: any;
  Description: any;
  
  
  constructor(public dialogRef: MatDialogRef<ProjectinfoComponent>, private salesforceService: SalesforceService,@Inject(MAT_DIALOG_DATA) public account: any) {}
  public chartOptions: ChartOptions = {
    responsive: true,
    maintainAspectRatio: false
  };

  public chartLabels: string[] = [];
  public chartData: ChartData<'doughnut', number[], string> = {
    labels: this.chartLabels,
    datasets: [
      {
        data: [],
        backgroundColor: ['rgba(245, 105, 39, 0.8)', 'rgba(245, 161, 39, 0.9)', 'rgba(245, 229, 39, 0.9)', 'rgba(255, 255, 0, 0.77)', 'rgba(249, 71, 9, 0.6)'],
        hoverBackgroundColor: ['rgba(255, 81, 0, 0.98)', 'rgba(255, 115, 0, 0.98)', 'rgba(245, 212, 0, 1)', 'rgba(234, 229, 0, 1)','rgba(250, 74, 10, 0.56)'],

      }
    ]
  };

  public chartType: ChartType = 'doughnut';

  
  ngOnInit() {
    this.salesforceService.getInfos(`SELECT Id, Name, theme__r.Name, Description__c, Team__r.Name, StartDate__c, EndDate__c FROM Project__c WHERE Id  ='${this.account.Id}'`)
    .subscribe(
      response => {
        this.Name = response.records[0].Name;
        this.Team = response.records[0].Team__r.Name;
        this.Description = response.records[0].Description__c;
        this.StartDate = response.records[0].StartDate__c;
        this.EndDate = response.records[0].EndDate__c;
        
        console.log(response.records[0]);
      },
      error => {
        console.error('Error retrieving number of projects:', error);
      }
          );

    this.salesforceService.getInfos(`SELECT Name FROM List__c WHERE Project__c = '${this.account.Id}'`).subscribe(
      response => {
        this.chartLabels = response.records.map((record: any) => record.Name);
  
        let totalTasks = 0;
        const dataPromises: Promise<any>[] = [];
  
        this.chartLabels.forEach((label, index) => {
          const dataPromise = this.salesforceService.getInfos(`SELECT COUNT(Id) FROM Talk__c WHERE List__c IN (SELECT Id FROM List__c WHERE Project__c = '${this.account.Id}' AND Name = '${label}')`).toPromise();
          dataPromises.push(dataPromise);
        });
  
        Promise.all(dataPromises)
          .then(responses => {
            responses.forEach((response, index) => {
              const taskCount = response.records[0].expr0;
              totalTasks += taskCount;
              const percentage = (taskCount / totalTasks) * 100;
              this.chartData.datasets[0].data[index] = response.records[0].expr0;
            });
            this.createdogChart();
  
            // Update the chart data
            this.chart.data.labels = this.chartLabels;
            this.chart.update();
          })
          .catch(error => {
            console.error('Error retrieving task counts:', error);
          });
      },
      error => {
        console.error('Error retrieving list names:', error);
      }
    );
    
    
    
  }
  createdogChart() {
    const chartdogCanvas = this.chartdogCanvas.nativeElement.getContext('2d');
  
    
  // Destroy the existing chart if it exists
  if (this.chart) {
    this.chart.destroy();
  }

  // Create the new chart
  this.chart = new Chart(chartdogCanvas, {
    type: 'doughnut',
    data: this.chartData,
    options: {
      ...this.chartOptions,
      plugins: {
        tooltip: {
          callbacks: {
            label: (context) => {
              const labelIndex = context.dataIndex;
              const listName = this.chartLabels[labelIndex];
              const taskCount = this.chartData.datasets[0].data[labelIndex];
              return `${listName}: ${taskCount} Tasks`;
            }
          }
        }
      }
    }
  });
}

}
