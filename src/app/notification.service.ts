import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class NotificationService {
  private notifications: { message: string; timestamp: Date }[] = [];
  notificationsUpdated: EventEmitter<{ message: string; timestamp: Date }[]> = new EventEmitter<{ message: string; timestamp: Date }[]>();

  constructor() {
    // Retrieve notifications from localStorage when the service is initialized
    const savedNotifications = localStorage.getItem('notifications');
    if (savedNotifications) {
      this.notifications = JSON.parse(savedNotifications);
    }
  }

  addNotification(message: string) {
    const notification = { message, timestamp: new Date() }; // Use new Date() to set the timestamp
    this.notifications.push(notification);
    this.notificationsUpdated.emit(this.notifications);
  
    localStorage.setItem('notifications', JSON.stringify(this.notifications));
  }
  

  getNotifications(): { message: string; timestamp: Date }[] {
    return this.notifications;
  }
}
