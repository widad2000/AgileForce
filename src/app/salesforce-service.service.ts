import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class SalesforceService {
  private apiKey = 'c92808a3142a2e2d13f13a0dbcd8d125';
  private accessTokenTrello = 'ATTA6c8e386a200eff1d080d255894d973eef573127991abc2ac995cb6137064ad2dBA078376';
  private apiUrl = 'https://api.trello.com/1';

  accessToken !: any ;
  instanceUrl !: any ;
  domain!:any;
  //private accessToken: string = '00D8d00000AYkcV!AQ0AQMC..N7CjkIBii897E2I6nDhK0tUsAKtjhk0W6KhWiniBX3ATQbTfQ.zjDDi1MWglQdues.axGjnjljxt_.rnEszAOI7';
  //private instanceUrl: string = 'https://its-1f-dev-ed.develop.my.salesforce.com';
  
  constructor(private http: HttpClient,private route: ActivatedRoute) { }

  getBoard(boardId: string) {
    const url = `${this.apiUrl}/boards/${boardId}?key=${this.apiKey}&token=${this.accessTokenTrello}`;
    return this.http.get(url);
  }
  getLists(boardId: string, ListName : any) {
    const url = `${this.apiUrl}/boards/${boardId}/lists`;
    const params = {
      key: this.apiKey,
      token: this.accessTokenTrello,
    };

    return this.http.get(url, { params });
  }

 /* createCard(cardData: any) {
    const url = `${this.apiUrl}/cards?key=${this.apiKey}&token=${this.accessToken}`;
    return this.http.post(url, cardData);
  }*/
  
  getList(listId: string) {
    const url = `${this.apiUrl}/lists/${listId}`;
    const params = {
      key: this.apiKey,
      token: this.accessTokenTrello,
    };

    return this.http.get(url, { params });
  }
  createBoard() {
    const url = `${this.apiUrl}/boards`;
    const params = {
      key: this.apiKey,
      token: this.accessTokenTrello,
      name: 'testy',
      desc: 'boardData description',
      // Add any other necessary parameters
    };

    return this.http.post(url, params);
  }
  createList(name: any,id:any) {
    const url = `${this.apiUrl}/lists`;
    const params = {
      key: this.apiKey,
      token: this.accessTokenTrello,
      name: name,
      idList:id,
      idBoard: id,
      // Add any other necessary parameters
    };
  console.log(params.idBoard)
    return this.http.post(url, params);
  }
  createCard(name: any,idList : any) {
    const params = {
      key: this.apiKey,
      token: this.accessTokenTrello,
     name : name,
      desc: name,
      idList: idList,
      // Add any other necessary parameters
    };

    return this.http.post(`${this.apiUrl}/cards`, params);
  }

  // Add more methods for other API interactions as needed.


  
  getDomain(): any {
    
    console.log(localStorage.getItem('instance_url')!=null); 
    console.log("ana",localStorage.getItem('instance_url'));
    console.log("tajribaaa1",    localStorage.getItem('reda'));
    
   if(localStorage.getItem('instance_url')==null ){
   
    this.route.queryParamMap.subscribe(params => {
      console.log("dkhallt getDomain params",params);
     // this.accessToken = params.get('access_token');
      this.domain = params.get('domain');
  
      // Do whatever you want with the accessToken and domain here
     // console.log('Access Token:', this.accessToken);
      console.log('Domain:', this.domain);
  
      // For example, you can store them in local storage for later use
      //localStorage.setItem('access_token', accessToken);
      //localStorage.setItem('domain', domain);
     

      if(this.domain!=null){
        localStorage.setItem('instance_url', this.domain);
       }
    });
   
 
   
   }
    

    return  "https://" + localStorage.getItem('instance_url');
 

     
    
    
  }
    
  getAccessToken(): any {
   
    console.log(localStorage.getItem('access_token')==null);
    console.log(localStorage.getItem('access_token'));
    console.log("tajribaaa",    localStorage.getItem('reda'));

    if(localStorage.getItem('access_token')==null  ){
     
      this.route.queryParamMap.subscribe(params => {

        console.log("dkhallt getAccessToken params",params);
        this.accessToken = params.get('access_token');
        // this.domain = params.get('domain');
     
         // Do whatever you want with the accessToken and domain here
        // console.log('Access Token:', this.accessToken);
         console.log('Domain:',  this.accessToken);
     
         // For example, you can store them in local storage for later use
         //localStorage.setItem('access_token', accessToken);
         //localStorage.setItem('domain', domain);
         if(this.accessToken!=null){
          localStorage.setItem('access_token', this.accessToken);
         }
        
       });
 
 

    }
   return localStorage.getItem('access_token');
  }
  private getHeaders(): HttpHeaders {
    const headers = new HttpHeaders({
      'Authorization': "Bearer "+ localStorage.getItem('access_token'),
      'Content-Type': 'application/json'
      
    });
    return headers;
  }
  public getInfoUserLog() {
    const url = "https://" +localStorage.getItem('instance_url')+`/services/oauth2/userinfo`;
    return this.http.get<any>(url, { headers: this.getHeaders() });
  }
  
  public getInfos(soql :string ): Observable<any> {
    const url = "https://" +localStorage.getItem('instance_url')+`/services/data/v58.0/query/?q=${soql}`;
    return this.http.get<any>(url, { headers: this.getHeaders() });
  }
  public createRecord(sObject: string, recordData: any): Observable<any> {
    const url = "https://" +localStorage.getItem('instance_url')+`/services/data/v58.0/sobjects/${sObject}`;
    return this.http.post<any>(url, recordData, { headers: this.getHeaders() });
  }

  public updateRecord(sObject: string, recordId: string, recordData: any): Observable<any> {
    const url = "https://" +localStorage.getItem('instance_url')+`/services/data/v58.0/sobjects/${sObject}/${recordId}`;
    return this.http.patch<any>(url, recordData, { headers: this.getHeaders() });
  }

  public deleteRecord(sObject: string, recordId: string): Observable<any> {
    const url = "https://" +localStorage.getItem('instance_url')+`/services/data/v58.0/sobjects/${sObject}/${recordId}`;
    return this.http.delete<any>(url, { headers: this.getHeaders() });
  }
  public description(sobjects: string){
    const url = "https://" +localStorage.getItem('instance_url')+`/services/data/v58.0/sobjects/${sobjects}/describe`;
    return this.http.get<any>(url, { headers: this.getHeaders() });
  }
}