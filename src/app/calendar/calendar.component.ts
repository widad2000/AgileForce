import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { CalendarOptions, EventInput } from "@fullcalendar/core";
import dayGridPlugin from '@fullcalendar/daygrid'; 
import { SalesforceService } from "../salesforce-service.service";
import { Calendar } from '@fullcalendar/core';

import { AddEventDialogComponent } from "../add-event-dialog/add-event-dialog.component";

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit {
  infouser !: any;
  calendarEvents: EventInput[] = [
    { title: 'Event Now', start: new Date() }
];
  calendarOptions: CalendarOptions = {
    plugins: [dayGridPlugin],
    
    initialView: 'dayGridMonth',
    weekends: true,
    editable: true,
    selectable: true,
    selectMirror: true,
    dayMaxEvents: true,
    
    events: [] // Initialize events as an empty array
  };
  
  
  handleDateClick(arg : any) {
    
    alert('date click! ' + arg.dateStr)
  }

  constructor(private salesforceService: SalesforceService, private dialog: MatDialog) {}

  ngOnInit() {
    this.salesforceService.getInfoUserLog().subscribe(res=>{
      
      this.infouser = res;
      
    });
    
    
    setTimeout(() => {
      return this.salesforceService.getInfos(`SELECT Subject, Description, EndDateTime, StartDateTime FROM Event WHERE OwnerId='${this.infouser.user_id}'`)
      .subscribe(
        response => {
          console.log("new ",response)
          const events = response.records.map((event: any) => ({
            title: event.Subject,
            start: new Date(event.StartDateTime),
            end: new Date(event.EndDateTime),
            description: event.Description
          }));
          

          // Update the calendar events
          this.calendarOptions.events = events;
          
          
        },
        error => {
          console.error('Error retrieving events:', error);
        }
      );
    }, 2200);
    setTimeout(() => {
     
        this.calendarOptions = {
          initialView: 'dayGridMonth',
          events: this.calendarOptions.events,
          dateClick: this.onDateClick.bind(this),
        };
      
    }, 2500);
  }
  onDateClick(res: any) {
    alert('Clicked on date : ' + res.dateStr);
    console.log(res)
  }

  

  openPopup() {
    const dialogRef = this.dialog.open(AddEventDialogComponent, {
      width: '500px',
      data: event
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
    });
  }

  
}
