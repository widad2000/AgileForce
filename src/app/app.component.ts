import { Component, OnInit } from '@angular/core';
import { SalesforceService } from './salesforce-service.service';
import { MatDialog } from '@angular/material/dialog';
import { AddprojectComponent } from './addproject/addproject.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
}
