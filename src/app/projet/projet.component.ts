import { Component, OnInit } from '@angular/core';
import { SalesforceService } from '../salesforce-service.service';
import { MatDialog } from '@angular/material/dialog';
import { AddprojectComponent } from '../addproject/addproject.component';
import { ProjectinfoComponent } from '../projectinfo/projectinfo.component';
import { Router } from '@angular/router';
import { forkJoin } from 'rxjs';
import { Priority } from '../list/add-user-story/add-user-story.component';


@Component({
  selector: 'app-projet',
  templateUrl: './projet.component.html',
  styleUrls: ['./projet.component.css']
})
export class ProjetComponent implements OnInit {
  LastList : any[] = [];
  public accounts!: any[];
  public noaccounts!: any[];
  public filteredAccounts!: any[];
  public filterednoAccounts!: any[];
  displayMyProjects: boolean = true;
  isSubmitting: boolean = false;
  IdAccount!: string;

  constructor(private router: Router, private salesforceService: SalesforceService, private dialog: MatDialog) {
    console.log("constructor projet ");
    this.salesforceService.getAccessToken();
    this.salesforceService.getDomain();


  }
  

  openFieldFormDialog() {
    this.isSubmitting = true;
    const dialogRef = this.dialog.open(AddprojectComponent, {
      width: '500px'
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      this.isSubmitting = false;
    });
  }
  
  getColor(priority: string) {
    let color: string;
    switch (priority) {
    case 'High':
    color = '#F8702B';
    break;
    case 'Medium':
    color = '#FCB424';
    break;
    case 'Low':
    color = '#FCD432';
    break;
    case 'Lowest':
    color = '#FFE95D';
    break;
    case 'Highest':
    color = '#C1470A';
    break;
    default:
    color = '';
    break;
    }
    return color;
    }

  searchProjects(event: any) {
    const searchValue = event.target.value;
    console.log(searchValue)
    
    if (searchValue.trim() === '') {
      this.LastList = this.filteredAccounts;
      this.filterednoAccounts = this.noaccounts;
      
    } else {
      this.LastList = this.filteredAccounts.filter(account =>
        account.Name.toLowerCase().includes(searchValue.toLowerCase())
        
      );
      console.log(this.LastList)
      
      this.filterednoAccounts = this.noaccounts.filter(noaccount =>
        noaccount.Name.toLowerCase().includes(searchValue.toLowerCase())
      );
      console.log(this.filterednoAccounts)
    }
    
  }

  chart(IdProjet: string){
    
  }
 

  ngOnInit() {
    
    
    

    this.showMyProjects();
  
    
  }

  
  showMyProjects() {
    this.salesforceService.getInfoUserLog().subscribe(
      response => {
        this.IdAccount = response.user_id;
        console.log(this.IdAccount);

        this.salesforceService.getInfos(`SELECT Id, Progress__c, Name, theme__r.Name, Description__c, Team__r.Name, Team__r.Id, StartDate__c, EndDate__c, Priority__c FROM Project__c WHERE Team__c IN (SELECT Team__c FROM UserTeam__c WHERE User__r.Id = '${this.IdAccount}')`)
          .subscribe(
            response => {
              this.displayMyProjects = true;
              this.accounts = response.records;
              
             
              console.log(this.accounts);
              const requests = this.accounts.map(a =>
                this.salesforceService.getInfos(`SELECT User__r.Id, User__r.SmallPhotoUrl, Team__c, User__r.Name FROM UserTeam__c Where Team__c='${a.Team__r.Id}'`)
              );
      
              forkJoin(requests).subscribe(
                responses => {
                  this.filteredAccounts = [];
                  responses.forEach((response, index) => {
                    const b = {
                      Id: this.accounts[index].Id,
                      Name: this.accounts[index].Name,
                      Team: this.accounts[index].Team__r.Name,
                      Description: this.accounts[index].Description__c,
                      Priority: this.accounts[index].Priority__c,
                      Progress: this.accounts[index].Progress__c,
                      Tasks: response.records
                    };
                    this.filteredAccounts.push(b);
                    
                    
                  });
                  console.log(this.LastList);
                  this.LastList = this.filteredAccounts;
                },
                error => {
                  console.error('Error updating talk:', error);
                }
              );
              

         
            },
            error => {
              console.error('Error retrieving accounts:', error);
            }
          );
      },
      error => {
        console.error('Error retrieving user info:', error);
      }
    );
    
    }
    showOtherProjects() {
      this.salesforceService.getInfoUserLog().subscribe(
        response => {
          this.IdAccount = response.user_id;
          console.log(this.IdAccount);
  
          this.salesforceService.getInfos(`SELECT Id, Name, theme__r.Name, Description__c, Team__r.Name, StartDate__c, EndDate__c FROM Project__c `)
            .subscribe(
              respons => {
                this.displayMyProjects = false;
                this.noaccounts = respons.records;
                
                this.filterednoAccounts = this.noaccounts;
                console.log(this.noaccounts);
            
              },
              error => {
                console.error('Error retrieving accounts:', error);
              }
            );
        },
        error => {
          console.error('Error retrieving user info:', error);
        }
      );
      
      }
  openPopupT(account: any) {
    console.log('hhr'+account);
    const dialogRef = this.dialog.open(ProjectinfoComponent, {
      width: '700px',height: '650px',
      data: account
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
    });
}

  GoToDetail(IdProjet : any){
    console.log('test');
    this.router.navigate(['/list', IdProjet]);
  }
}
