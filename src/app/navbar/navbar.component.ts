import { Component, OnInit  } from '@angular/core';
import { SalesforceService } from '../salesforce-service.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';
import { NotificationService } from '../notification.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit  {
  profile!: string;
  Name!: string;
  email!: string;
  given_name!: string;
  picture !: string;
  currentRoute: string = '';
  isTagSelected: boolean = false;
  isNotificationActive: boolean = false;
  notifications: { message: string; timestamp: Date }[] = [];
  lastCreatedTaskName: string | undefined;
  showAllNotifications: boolean = false;
  


 


  constructor(private snackBar: MatSnackBar, private salesforceService: SalesforceService, private router: Router,private notificationService: NotificationService){}
  showNotification(): void {
    this.snackBar.open('Ceci est une notification !', 'Fermer', {
      duration: 5000 // Durée en millisecondes (5 secondes dans cet exemple)
    });
  }
  onTagSelected() {
    this.isTagSelected = true;
  }
  ngOnInit() {
    this.notifications = this.notificationService.getNotifications();
    this.notificationService.notificationsUpdated.subscribe((notifications: { message: string; timestamp: Date }[]) => {
      // Update the notifications list
      this.notifications = notifications;
      console.log(this.notifications)
     
      this.isNotificationActive = true;
    });

    // Initialize the notifications list
    
    
   // localStorage.getItem('notification');
    this.salesforceService.getInfoUserLog().subscribe(
      (response) => {
        console.log(response);
        this.Name = response.family_name;
        this.email = response.email;
        this.given_name = response.given_name;
        this.picture = response.picture;
       
      },
      (error) => {
        console.error('Error:', error);
      }
    );

    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ).subscribe(() => {
      this.currentRoute = this.getCurrentRouteName(this.router.url);
    });
    
    
    
  }
  ngAfterViewInit() {
    console.log("isNotificationActive:", this.isNotificationActive);
  }
  toggleShowAllNotifications() {
    this.showAllNotifications = !this.showAllNotifications;
  }
  formatTime(timestamp: Date | string): string {
    const date = timestamp instanceof Date ? timestamp : new Date(timestamp);
  
    if (!isNaN(date.getTime())) {
      return date.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });
    } else {
      return ''; // Return an empty string or some default value if the timestamp is not valid.
    }
  }
  
  private getCurrentRouteName(url: string): string {
    
    const routeNameMap: { [key: string]: string } = {
      '/dashboard': 'Dashboards',
      '/projet': 'Projects',
      '/team' : 'Teams',
      '/Profil' : 'Profil',
      '/calendar' : 'Calendar'
      
    };
        
    return routeNameMap[url] || 'Board' ;
  }
  
}