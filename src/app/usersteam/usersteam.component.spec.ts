import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersteamComponent } from './usersteam.component';

describe('UsersteamComponent', () => {
  let component: UsersteamComponent;
  let fixture: ComponentFixture<UsersteamComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [UsersteamComponent]
    });
    fixture = TestBed.createComponent(UsersteamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
