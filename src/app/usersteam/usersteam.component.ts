import { Component, Inject } from '@angular/core';
import { SalesforceService } from '../salesforce-service.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AdduserComponent } from '../adduser/adduser.component';


@Component({
  selector: 'app-usersteam',
  templateUrl: './usersteam.component.html',
  styleUrls: ['./usersteam.component.css']
})
export class UsersteamComponent {
  public Users!: any[];
  public Projets! : any[];
  public userteams !: any [];

  constructor(private salesforceService: SalesforceService, private MatDialog : MatDialog, private dialog: MatDialogRef<UsersteamComponent>, @Inject(MAT_DIALOG_DATA) public team: any) {}
  
  ngOnInit() {
    
    //lister useres affecter a cette team
    
        this.salesforceService.getInfos(`select Id, Name, FullPhotoUrl from User where Id IN (SELECT User__r.Name, Role__c, Description__c  from UserTeam__c where Team__c ='${this.team.Id}')`)
          .subscribe(
            response => {
              this.Users = response.records;
              console.log(this.Users);
            },
            error => {
              console.error('Error retrieving Teams:', error);
            }
          );
      
      //lister les projet affecter a cette team deja fait et en cours 
            
      this.salesforceService.getInfos(`select Name, StartDate__c, EndDate__c  from Project__c where Team__c='${this.team.Id}'`)
      .subscribe(
        response => {
          this.Projets = response.records;
        },
        error => {
          console.error('Error retrieving Teams:', error);
        }
      );
  }

  deleteTeam(idteam: any): void {
    console.log(idteam);
    
    this.salesforceService.deleteRecord('Team__c', idteam).subscribe(
      response => {
        console.log('Record deleted successfully');
        window.location.reload();
      },
      error => {
        console.error('Error deleting List__c record:', error);
      }
    );
  
    this.salesforceService.getInfos(`select Id, User__c, Team__c from UserTeam__c`).subscribe(
      response => {
        this.userteams = response.records;
  
        this.userteams.forEach(userTeam => {
          if (userTeam.Team__c === idteam) {
            this.salesforceService.deleteRecord('UserTeam__c', userTeam.Id).subscribe(
              response => {
                console.log('UserTeam__c record deleted successfully');
              },
              error => {
                console.error('Error deleting UserTeam__c record:', error);
              }
            );
          }
        });
      },
      error => {
        console.error('Error retrieving UserTeam__c records:', error);
      }
    );
  }

  deletuserfromteam(deletUser: any, teamId : any ){
    
    this.salesforceService.getInfos(`select Id from UserTeam__c where User__c = '${deletUser}' and Team__c = '${teamId}'`).subscribe(resp=>{
      const userTeamIds = resp.records.map((record: any) => record.Id);
      console.log(userTeamIds);
      this.salesforceService.deleteRecord('UserTeam__c', userTeamIds).subscribe(
        response => {
          console.log('UserTeam__c record deleted successfully');
          window.location.reload();

        },
        error => {
          console.error('Error deleting UserTeam__c record:', error);
        }
      );
    }) ;
  }
  

  openPopup(team: any) {
    const dialogRef = this.MatDialog.open(AdduserComponent, {
      width: '900px',
      data: team
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
    });
}
}
