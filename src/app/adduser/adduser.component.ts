import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { AddTeamComponent } from '../add-team/add-team.component';
import { SalesforceService } from '../salesforce-service.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-adduser',
  templateUrl: './adduser.component.html',
  styleUrls: ['./adduser.component.css']
})
export class AdduserComponent {
  isSubmit: boolean = false;
  record: any = {};
  myForm!: FormGroup; 
  public Urs!: any[];
  selectedOption!: string;

  constructor(public dialogRef: MatDialogRef<AddTeamComponent>, private salesforceService: SalesforceService,@Inject(MAT_DIALOG_DATA) public Team: any) {}
  ngOnInit() {
    
    this.myForm = new FormGroup({
     
      User__c: new FormControl(''),
      Description__c : new FormControl(''),
      Role__c : new FormControl('')
      
    });
    

    this.salesforceService.getInfos(`select Id, Name from User where Id NOT IN (SELECT User__c from UserTeam__c where Team__c ='${this.Team.Id}')`)
          .subscribe(response => {
            
            this.Urs = response.records.map((team: any) => ({
              id: team.Id,
              name: team.Name
            }));
            },
            error => {
              console.error('Error retrieving accounts:', error);
            }
          );
    
  }
  submitForm() {
    if (this.myForm.valid) {
      const formData = this.myForm.value;
      this.isSubmit = true;
      const selectedUsers = this.myForm.value.User__c ;
      console.log('fhhfh'+this.myForm.value.User__c)
      console.log('role',this.myForm.value.Role__c)
      console.log('role',this.myForm.value.Description__c)

  
            if (selectedUsers.length > 0) {
              // Insert the UserTeam__c records for each selected user
 //             selectedUsers.forEach((userId: string) => {
                const userTeamRecord = {
                  User__c: selectedUsers,
                  Team__c: this.Team.Id,
                  Description__c : this.myForm.value.Description__c,
                  Role__c : this.myForm.value.Role__c,
                  
                };
                console.log('records',userTeamRecord)
  
                // Insert the UserTeam__c record
                this.salesforceService.createRecord('UserTeam__c', userTeamRecord)
                  .subscribe(
                    () => {
                      console.log('UserTeam__c record added successfully');
                      this.isSubmit = false;
                      this.dialogRef.close();
                    },
                    error => {
                      console.error('Error adding UserTeam__c record:', error);
                      this.isSubmit = false;
                    }
                  );
              
          //        window.location.reload();
            //    });
              console.log('hfbnjdnjd'+selectedUsers)
            } else {
              // No users selected, close the dialog
              this.isSubmit = false;
              this.dialogRef.close();
            }
}}
closeDialog() {
  this.dialogRef.close();
}}
